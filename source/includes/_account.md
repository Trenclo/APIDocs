# Account

## Get All Users

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
GET /v1/users/ HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/users/" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [
    {
      "userid": "1a2b3c4d",
      "tenantid": "1a2b3c4d",
      "emailaddress": "john@doe.com",
      "fullname": "John Doe",
      "usertype": "Admin|Partner|User",
      "language": "en-gb",
      "timezone": "Europe/London",
      "iconfile": "data:uri",
      "emailverified": true,
      "timecreated": "2021-01-01 00:00:00.00000 GMT",
      "timemodified": "2021-01-01 00:00:00.00000 GMT"
    }
  ]
}
```

This endpoint returns a list of all users in the current tenant.

## Get Current User

<aside class="notice">This resource requires the <code>user</code> scope.</aside>

```http
GET /v1/users/me HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/users/me" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint returns the user object for the owner of the current authorization token.

## Get User by ID

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
GET /v1/users/1a2b3c4d HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/users/1a2b3c4d" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint returns a user by ID.

## Update User

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>



## Create User

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
POST /v1/users/ HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/users/" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint creates a new user.

## Delete User

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
DELETE /v1/users/1a2b3c4d HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/users/1a2b3c4d" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint deletes a user by ID.

## Get All Tenants

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
GET /v1/tenants/ HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/tenants/" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint returns a list of all tenants.

## Get Current Tenant

<aside class="notice">This resource requires the <code>user</code> scope.</aside>

```http
GET /v1/tenants/me HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/tenants/me" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint returns the tenant object for the owner of the current authorization token.

## Get Tenant by ID

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
GET /v1/tenants/1a2b3c4d HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/tenants/1a2b3c4d" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint returns a tenant by ID.

## Update Tenant

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>


## Create Tenant

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
POST /v1/tenants/ HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/tenants/" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint creates a new tenant.

## Delete Tenant

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
DELETE /v1/tenants/1a2b3c4d HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/tenants/1a2b3c4d" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint deletes a tenant by ID.

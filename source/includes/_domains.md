# Domain Names

## Get All Domains

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
GET /v1/resources/domains/ HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/resources/domains/" \
  -H "Authorization: Bearer 1a2b3c4d"
```

```javascript
$.ajax({
  'url': 'https://api.<REGION>.trenclo.com/v1/resources/domains/',
  'method': 'GET',
  'dataType': 'json',
  beforeSend: function(){

  },
  success: function(data){

  },
  error: function(){

  }
});
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Get Domain by ID

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
GET /v1/resources/domains/1a2b3c4d HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/resources/domains/1a2b3c4d" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Update Domain by ID

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>


## Create Domain

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
POST /v1/resources/domains/ HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/resources/domains/" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Delete Domain

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
DELETE /v1/resources/domains/1a2b3c4d HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/resources/domains/1a2b3c4d" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Get All DNS Records for Domain

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
GET /v1/resources/domains/1a2b3c4d/dns/ HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/resources/domains/1a2b3c4d/dns/" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Get DNS Record by ID

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>


## Update DNS Record by ID

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>


## Create DNS Record

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>


## Delete DNS Record by ID

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>


## Get All Domain Contacts

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
GET /v1/resources/domaincontacts/ HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/resources/domaincontacts/" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Get Domain Contact

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>


## Update Domain Contact

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>


## Create Domain Contact

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>


## Delete Domain Contact

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>


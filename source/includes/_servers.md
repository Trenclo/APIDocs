# Server Hosting

## Get All Servers

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
GET /v1/resources/servers/ HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/resources/servers/" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Get Server by ID

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
GET /v1/resources/servers/1a2b3c4d HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/resources/servers/1a2b3c4d" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Update Server by ID


## Create Server

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
POST /v1/resources/servers/ HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/resources/servers/" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Delete Server

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
DELETE /v1/resources/servers/1a2b3c4d HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/resources/servers/1a2b3c4d" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint
# Billing

## Get All Invoices

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
GET /v1/billing/invoices/ HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/billing/invoices/" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Get Invoice by ID

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
GET /v1/billing/invoices/1a2b3c4d HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/billing/invoices/1a2b3c4d" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Get All Payment Methods

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
GET /v1/billing/paymentmethods/ HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/billing/paymentmethods/" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Get Payment Method by ID

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
GET /v1/billing/paymentmethods/1a2b3c4d HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/billing/paymentmethods/1a2b3c4d" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Update Payment Method by ID

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>


## Create Payment Method

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
POST /v1/billing/paymentmethods/ HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/billing/paymentmethods/" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Delete Payment Method

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
DELETE /v1/billing/paymentmethods/1a2b3c4d HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/billing/paymentmethods/1a2b3c4d" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Get All Subscriptions

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
GET /v1/billing/subscriptions/ HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/billing/subscriptions/" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Get Subscription by ID

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
GET /v1/billing/subscriptions/1a2b3c4d HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/billing/subscriptions/1a2b3c4d" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Update Subscription by ID

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>


## Create Subscription

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
POST /v1/billing/subscriptions/ HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/billing/subscriptions/" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Delete Subscription

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>

```http
DELETE /v1/billing/subscriptions/1a2b3c4d HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/billing/subscriptions/1a2b3c4d" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Get All Licenses

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>


## Get All Licenses for a User

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>


## Get License by ID

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>


## Update License

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>


## Create License

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>


## Delete License

<aside class="notice">This resource requires the <code>admin</code> or <code>partner</code> scope.</aside>


## Get All Products

<aside class="notice">This resource requires the <code>public</code> scope.</aside>

```http
GET /v1/billing/products/ HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/billing/products/" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Get Product by ID

<aside class="notice">This resource requires the <code>public</code> scope.</aside>

```http
GET /v1/billing/products/1a2b3c4d HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/billing/products/1a2b3c4d" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Get All Plans for a Product

<aside class="notice">This resource requires the <code>public</code> scope.</aside>

```http
GET /v1/billing/products/1a2b3c4d/plans/ HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/billing/products/1a2b3c4d/plans/" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

## Get Product Plan by ID

<aside class="notice">This resource requires the <code>public</code> scope.</aside>

```http
GET /v1/billing/products/1a2b3c4d/plans/1a2b3c4d HTTP/2.0
Host: api.<REGION>.trenclo.com:443
Authorization: Bearer 1a2b3c4d
```

```shell
curl "https://api.<REGION>.trenclo.com/v1/billing/products/1a2b3c4d/plans/1a2b3c4d" \
  -H "Authorization: Bearer 1a2b3c4d"
```

> This endpoint returns JSON structured like this:

```json
{
  "success": true,
  "data": [

  ]
}
```

This endpoint

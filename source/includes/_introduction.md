# Introduction

## Overview

Welcome to the Trenclo API!

We have language bindings in HTTP, Shell and jQuery AJAX. You can view code examples in the dark area to the right, and you can switch the programming language of the examples with the tabs in the top right.

This API documentation page was created with [Slate](https://github.com/slatedocs/slate).

This documentation is open source. If you spot an error or something which could be more clearly worded, submit an issue or pull request on [the GitLab repo](https://gitlab.com/Trenclo/APIDocs).

<aside class="notice">This API is still in beta and is not considered ready for production use; endpoints and parameters are subject to change.</aside>

## Quick Start Examples

### HTML5 / Javascript

Check out the source code for the [Trenclo Admin app](https://gitlab.com/Trenclo/TrencloApp) on GitLab to understand how we've implemented this API in a cross-platform HTML5 / Javascript application.

### Bash / Shell / cURL

Find examples for `curl` on the command line throughout this documentation. The easiest way to get started testing API calls is to create an API key in the Trenclo Admin app and send the Authorization header as `Authorization: ApiKey <Your-API-Key>`.

## Regions

The API operates in multiple segregated regions. You should connect to the region on which the account was created. The available regions are as follows:

Region Code | Region Name    | Data Location   | Availability
------------|----------------|-----------------|--------------
Test        | Sandbox Mode   | United Kingdom  | ✅ Live
GB          | United Kingdom | United Kingdom  | ✅ Live
...         | ...            | ...             | ⌛ Coming Soon

## Errors

The API uses the following HTTP response codes:

> Unsuccessful requests will always return a JSON body in the following format:

```json
{
  "success": false,
  "error": {
    "exceptionText": "User not created. Missing field `emailaddress` from request.",
    "userMessage": "An error occured while attempting to create the user, please check your input and try again."
  },
  "data": []
}
```

Code  | Message               | Description
----- | --------------------- | -------------------------------------------------------------------
400   | Bad Request           | Your request is invalid.
403   | Forbidden             | The resource requested is forbidden.
404   | Not Found             | The specified resource could not be found.
429   | Too Many Requests     | You're requesting too many resources! Slow down!
500   | Internal Server Error | We had a problem with our server. Try again later.
503   | Service Unavailable   | We're temporarily offline for maintenance. Please try again later.

# Authentication

## Overview

> When using an API key, set a header similar to the following:

```
Authorization: ApiKey <Your-API-Key>
```

> When using a Bearer token, set a header similar to the following:

```
Authorization: Bearer <Your-Bearer-Token>
```

The Trenclo API supports OAuth2.0 (for actions on behalf of a user) and API Key authorization (for automation scenarios).

## OAuth2.0 Authorization



## API Key Authorization

 - Create an API key in the Trenclo Admin app.
 - Send the `Authorization` header with the value `ApiKey <Your-API-Key>`.

## Partner Tenant Access

Partner tenants can perform API actions on behalf of tenants they manage.

 - Send the `X-Tenant-Id` header with the value as the tenant's ID.
